#!/bin/sh

set -e

iptables -t nat -A PREROUTING -p tcp --dport ${ENTRYPORT} -j DNAT --to-destination  ${LBSERVICEIP}:${LBSERVICEPORT}
iptables -t nat -A POSTROUTING -j MASQUERADE
for iface in $(ip a | grep inet | grep eth | awk '{print $2}'); do
  iptables -t nat -A POSTROUTING -s "$iface" -j MASQUERADE
done

while [ true ];
do
    sleep 7200;
done
